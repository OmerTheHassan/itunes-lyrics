import sys
import urllib.request
import urllib.parse
import json
import re

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:64.0) Gecko/20100101 Firefox/64.0'

song_name = sys.argv[1]
song_name = re.sub(r'\([^)]+\)', '', song_name)
song_name = re.sub(r'\[[^\]]+\]', '', song_name)
song_name = song_name.strip()

song_url = 'https://genius.com/api/search/song?q=' + urllib.parse.quote(song_name)
request = urllib.request.Request(song_url)
request.add_header('Accept', 'application/json')
request.add_header('User-Agent', USER_AGENT)
response = urllib.request.urlopen(request)

with response:
    decoded_response = json.loads(response.readline())

results = decoded_response['response']['sections'][0]['hits']

if len(results) > 0:
    lyrics_url = results[0]['result']['url']

    request = urllib.request.Request(lyrics_url)
    request.add_header('User-Agent', USER_AGENT)
    response = urllib.request.urlopen(request)

    with response:
        lyrics_html = ''.join(line.decode() for line in response.readlines())
        match = re.search(r'<div class="lyrics">((.|\n)*?)</div>', lyrics_html)

        if match:
            print(re.sub(r'<[^>]*>', '', match.group(1)).strip())
