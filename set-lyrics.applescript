-- Usage: osascript set-lyrics.applescript [<overwrite=0|1>]

on run argv
    set overwrite to number of argv is 1 and item 1 of argv equals "1"

    tell application "Music"
        repeat with currentTrack in tracks
            tell currentTrack
                try
                    set query to artist & " " & name
                    log query

                    if lyrics equals "" or overwrite
                        set fetched_lyrics to (do shell script "python3 get-lyrics.py " & quoted form of query)
                        log fetched_lyrics
                        set lyrics to fetched_lyrics
                    else
                        log "---LYRICS ALREADY EXIST"
                    end if
                on error errStr number errorNumber
                    log "---ERROR: " & errStr
                end try
            end tell
        end repeat
    end tell
end run
